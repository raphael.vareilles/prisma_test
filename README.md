# webpack, ES6 and Babel for an user table application

This is a small project wrote for Prisma dev test.
Create a table with data from a JSON file containg a list of users.
We can filter the table with query parameters in the URL.
These parameters are : `eyeColor` and `age`.
eyeColor can be `blue`, `brown` or `green`.
age can be `20-25`, `26-30`, `31-35` or `36-41`.


example : `http://{localUrlPath}/?eyeColor=blue&age=20-25`

<img
  src="https://i.postimg.cc/t4VRs1V8/prisma-screen.png"
  alt="Alt text"
  title="Optional title"
  style="display: inline-block; margin: 0 auto; max-width: 400px">

## Requirements

* npm

## Installation

```
npm install
```

## Build with Webpack

```
npm run build
```

it will generate a `dist` folder with the `bundle.js` file.
the application is accessible at `http://{localUrlPath}/dist/index.html`

All right!

## References

* [author](https://gitlab.com/fourmislabs)
* [webpack](https://github.com/webpack/webpack)
* [Babel](https://github.com/babel/babel)