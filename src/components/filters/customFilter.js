/**
 * @description Create filters by query params
 * @returns {Array<{key: string, value:string}>} Array of filters as key value pair.
 * @example
 * [{
 * key: 'age',
 * value: '2025'
 * },
 * {
 * key: 'eyeColor',
 * value: 'blue'
 * }]
 */
export function createFiltersByQueryParams() {
  if(!window.location.search) {
    return [];
  }
  let url = window.location.search;
  url = url.split("?")[1];
  url = url.split("&");
  const params = url.map((param) => {
    return {
      key: param.split("=")[0],
      value: param.split("=")[1],
    };
  });
  return params;
}

/**
 * @description Filter data by filters
 * @param {Array<{name: {first: string, last: string}, company: string, age: number, eyeColor: string, email: string, phone: string}>} data Array of users
 * @param {Array<{key: string, value:string}>} filters Array of filters as key value pair.
 * @returns {Array<{name: {first: string, last: string}, company: string, age: number, eyeColor: string, email: string, phone: string}>} Filtered data
 * @example
 * [{
 * name: {
 * first: 'Lindsey',
 * last: 'Waters'
 * },
 * company: 'ZILLACOM',
 * age: 20,
 * eyeColor: 'blue',
 * email: 'lindsey.waters@protomail.com',
 * phone: '+1 (807) 568-3228'
 * }]
 *
 * [{
 * key: 'age',
 * value: '2025'
 * },
 * {
 * key: 'eyeColor',
 * value: 'blue'
 * }]
 */
export function filter(data, filters) {
  let _data = [...data];
  if (filters.length <= 0) {
    return _data;
  }
  filters.forEach((filter) => {
      if(filter.key === 'age') {
        const interval = sliceAgeInterval(filter.value);
        _data = _data.filter((user) => {
          return user[filter.key] >= interval[0] && user[filter.key] <= interval[1];
        });
        return;
      }
    _data = _data.filter((user) => {
      return user[filter.key] == filter.value;
    });
  });
  return _data;
}
/**
 * @description Slice age interval  to lower and upper interval
 * @param {number} $ageInterval
 * @returns {Array<string>} Array of lower and upper interval
 * @example
 * 2025 => [20, 25]
 * 3035 => [30, 35]
 */
function sliceAgeInterval($ageInterval) {
  const lowerInterval = parseInt($ageInterval).toString().slice(0, 2);
  const upperInterval = parseInt($ageInterval).toString().slice(2, 4);
  return [lowerInterval, upperInterval];
}

