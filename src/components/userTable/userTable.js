import userData from "../../json/datas.json" assert { type: 'json' };
import { filter, createFiltersByQueryParams } from "../filters/customFilter.js";
import "./user-table.css";

export default function userTable() {
  let _userData = [...userData];
  const cols = [
    "First name",
    "Last name",
    "Company",
    "Age",
    "Eye color",
    "E-mail",
    "phone",
  ];

  /**
   * @description Build table element with header and body.
   * @returns {HTMLTableElement} append empty table element to body of document.
   * @param {Array<string>} cols Array of columns name to be used as header of table.
   */
  function buildTable() {
    let table = document.createElement("table");
    table.setAttribute("id", "usersTable");

    let headerTable = table.createTHead();
    let headTr = headerTable.insertRow(0);

    cols.forEach((col) => {
      let th = document.createElement("th");
      th.innerHTML = col;
      headTr.appendChild(th);
    });

    document.body.appendChild(table);
    return table;
  }

  /**
   * @description Build table body with data.
   * @param {HTMLTableElement} table Table element to append data.
   * @param {Array<{name: {first: string, last: string}, company: string, age: number, eyeColor: string, email: string, phone: string}>} data Array of users.
   * @returns {HTMLTableElement} append data to table element.
  */
  function buildTableData(table, data) {
    if (data.length > 0) {
      return data.map((user) => {
        let tr = document.createElement("tr");
        let td = document.createElement("td");

        if (user.name.first) {
          td.innerHTML = user.name.first;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.name.last) {
          td.innerHTML = user.name.last;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.company) {
          td.innerHTML = user.company;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.age) {
          td.innerHTML = user.age;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.eyeColor) {
          td.innerHTML = user.eyeColor;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.email) {
          td.innerHTML = user.email;
          tr.appendChild(td);
          td = document.createElement("td");
        }

        if (user.phone) {
          td.innerHTML = user.phone;
          tr.appendChild(td);
        }

        table.appendChild(tr);

      });
    } else {
      let tr = document.createElement("tr");
      let td = document.createElement("td");
      td.innerHTML = "No data found";
      td.setAttribute("colspan", cols.length);
      td.setAttribute("style", "text-align: center;");
      tr.appendChild(td);
      table.appendChild(tr);
    }
  }

  let queries = createFiltersByQueryParams();
  _userData = filter(_userData, queries);
  let table = buildTable();
  buildTableData(table, _userData);
}
